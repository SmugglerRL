import graphix;
import colour;
import item;
import map;
import constants;
import glyph;
import stdlib;
import rng;


abstract class Being {
	int i;
	private bool gender;
	ubyte xlvl;
	int xp, hp = 30, maxhp, mp, maxmp;
	Vector2n loc;
	RGBColour fgcolour, bgcolour;
	Glyph glyph;
	bool light_source;
	int fov_dist, light_dist;
	Item[] inventory;
	Lightmap lightmap;
	RGBColour lightclr;

	private struct _attr { bool reverse, italic, bold, underline; }
	_attr attrs;

	pragma(inline, true) {
		pure void inchp(int amount=1) {
			if ((amount + hp) > maxhp) {
				hp = maxhp;
			} else {
				hp += amount;
			}
		}
		pure bool ismale() {
			return gender;
		}
		pure bool isfemale() {
			return !gender;
		}
	}

	Action getaction();

	this() {
		lightmap = new Lightmap(&loc, fov_dist*2 + 1);
	}
}

class Mon: Being {
	override Action getaction() {
		return rnd(Action.MoveNorth, Action.MoveSouthRight);
	}
	this() {
		fgcolour = RGBColour(0x00ff00);
		bgcolour = RGBColour(0xff00ff);
		attrs.reverse = true;
		this.glyph = Glyph.r;

		this.light_source = true;
		this.fov_dist = 4;
		this.light_dist = 4;

		this.lightclr = RGBColour(0x008800);

		super();
	}
}

class You: Being {
	Graphics1 graphics;
	this(Graphics1 g) {
		fgcolour = RGBColour(0xffffff);
		bgcolour = RGBColour(0x000000);
		attrs.reverse = true;

		this.graphics = g;
		this.glyph = Glyph.at;

		this.light_source = true;
		this.fov_dist = 24;
		this.light_dist = 12;

		//this.lightclr = RGBColour(0xee9933);
		this.lightclr = RGBColour(0xff0000);

		super();
	}
	override Action getaction() {
		static immutable auto vikeys = set!dchar('h', 'j', 'k', 'l', 'y', 'u', 'b', 'n', 'q', '.', 'w');

		Action tmp;
		dchar c;

		while ((c = graphics.getch()) !in vikeys) {}

		switch (c) {
			case 'h':
				tmp = Action.MoveLeft;
				break;
			case 'l':
				tmp = Action.MoveRight;
				break;
			case 'j':
				tmp = Action.MoveSouth;
				break;
			case 'k':
				tmp = Action.MoveNorth;
				break;
			case 'y':
				tmp = Action.MoveNorthLeft;
				break;
			case 'u':
				tmp = Action.MoveNorthRight;
				break;
			case 'b':
				tmp = Action.MoveSouthLeft;
				break;
			case 'n':
				tmp = Action.MoveSouthRight;
				break;

			/+ TODO
			case 'H':
				tmp = Action.AttackLeft;
				break;
			case 'L':
				tmp = Action.AttackRight;
				break;
			case 'J':
				tmp = Action.AttackSouth;
				break;
			case 'K':
				tmp = Action.AttackNorth;
				break;
			case 'Y':
				tmp = Action.AttackNorthLeft;
				break;
			case 'U':
				tmp = Action.AttackNorthRight;
				break;
			case 'B':
				tmp = Action.AttackSouthLeft;
				break;
			case 'N':
				tmp = Action.AttackSouthRight;
				break;
			+/

			case 'q':
				tmp = Action.Quit;
				break;
			case '.':
				tmp = Action.Wait;
				break;
			case 'w': // (w)here am I?
				tmp = Action.Printloc;
				break;
			case ',':
				tmp = Action.Pickup;
				break;
			case 'i':
				tmp = Action.Showinv;
				break;
			default: assert(0);
		}
		return tmp;
	}
}

// Name credit to lrogue
ref Being at(list!Being mons, uint y, uint x) {
	foreach (ref tmpmon; mons) {
		if ((tmpmon.loc.y == y) && (tmpmon.loc.x == x)) {
			return tmpmon;
		}
	}
	// Evil hackery so we can return null
	static Being tmp = null;
	return tmp;
}
ref Being at(list!Being mons, Vector2n loc) {
	return mons.at(loc.y, loc.x);
}

