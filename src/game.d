import BearLibTerminal;

import stdlib;

import colour;
import constants;
import myfov;
import glyph;
import graphix;
import graphix0;
import item;
import light;
import logging;
import mapgen;
import map;
import rng;
import tile;
import util;
import item;
import being;

Graphics1 init_gfx() {
	return new Default_chargfx!SDL0("SmugglerRL!");
}

Map init_map() {
	return genmap(MapType.caves);
}
list!Being init_mons(Map map, Graphics1 graphics) {
	list!Being ret;
	ret ~= new You(graphics);
	foreach (_; 1 .. 1000) {
		ret ~= new Mon();
	}

	// If we didn't have this, then if (0, 0) was walkable, then you would automatically get placed there
	foreach (ref m; ret) {
		do {
			m.loc = Vector2n(rn1(map_y+1), rn1(map_x+1));
		} while ((!map[m.loc].walkable) || !ret.at(m.loc));
	}

	return ret;
}

list!Item init_items(Map map) {
	list!Item ret;

	foreach (_; 1 .. 1000) {
		ret ~= Item(choice(all_items));
	}
	foreach (ref i; ret) {
		do {
			i.loc = Vector2n(rn1(map_y+1), rn1(map_x+1));
		} while (!map[i.loc].walkable);
	}

	return ret;
}

void mainloop() {
	/***************\
	*   INIT CODE   *
	\***************/

	Being u;
	list!Being mons;
	list!Item items;
	Graphics1 graphics;
	Map map;

	graphics = init_gfx();
	map = init_map();

	mons = init_mons(map, graphics);
	u = mons.front;

	items = init_items(map);



	/***************\
	*   MAIN LOOP   *
	\***************/

	bool moving;
	int deltax, deltay;
	Action act;
	foreach (ref mon; mons) {
		do_fov(map, mon.lightmap, mon.loc.x, mon.loc.y, mon.fov_dist * mon.fov_dist, mon.light_dist * mon.light_dist);
	}
	do_colour(map, mons);

	graphics.refresh(u.loc.y, u.loc.x, mons, items, map);
	void handlemove(int yshift, int xshift, ref Being b) {
		if (map[b.loc.y+yshift, b.loc.x+xshift].walkable && !mons.at(b.loc.y+yshift, b.loc.x+xshift)) {
			b.loc += Vector2n(yshift, xshift);
		} else {
			// only when *you* walk into a wall
			if (cast(You)b) {
				graphics.pline("there is a thingy there!");
			}
		}
	}
outerloop:
	while (true) { foreach (ref Being mon; mons) {
		   act = mon.getaction();
		   // If we're moving, then move!
		   if ((Action.MoveNorth <= act) && (act <= Action.MoveSouthRight)) {
			   moving = true;
		   } else {
			   moving = false;
		   }
		   switch(act) {
			   case Action.MoveNorth:
				   deltay = -1;
				   break;
			   case Action.MoveSouth:
				   // ayyy
				   deltay = 1;
				   break;
			   case Action.MoveLeft:
				   deltax = -1;
				   break;
			   case Action.MoveRight:
				   deltax = 1;
				   break;
			   case Action.MoveNorthLeft:
				   deltay = deltax = -1;
				   break;
			   case Action.MoveNorthRight:
				   deltay = -1;
				   deltax = 1;
				   break;
			   case Action.MoveSouthLeft:
				   deltay = 1;
				   deltax = -1;
				   break;
			   case Action.MoveSouthRight:
				   deltay = deltax = 1;
				   break;
			   case Action.Wait:
				   break;
			   case Action.Quit:
				   break outerloop;
			   case Action.Printloc:
				   graphics.pline("(%s, %s)", mon.loc.x, mon.loc.y);
				   break;
			   default: assert(0);
		   }


		   if (moving) {
			   handlemove(deltay, deltax, mon);
		   }

		   deltay = deltax = 0;
	   }
		foreach (ref mon; mons) {
			do_fov(map, mon.lightmap, mon.loc.x, mon.loc.y, mon.fov_dist * mon.fov_dist, mon.light_dist * mon.light_dist);
		}
		do_colour(map, mons);

		graphics.refresh(u.loc.y, u.loc.x, mons, items, map);
		}



	/******************\
	*                  *
	*   CLEANUP CODE   *
	*                  *
	\******************/
	graphics.close();
}



/+
void parseargs(string[] args) {
	if (args.length == 2) {
		if (args[1] == "dumpmap") {
			dumpmap();
			exit(0);
		} else if (args[1] == "dumphtmlmap") {
			dumphtmlmap();
			exit(0);
		}
	}
}


void dumpmap() {
	import std.stdio;
	foreach (row; map.get_tiles) {
		foreach (tile; row) {
			write(cast(dchar)tile.glyph);
		}
		writeln();
	}
}
void dumphtmlmap() {
	import std.stdio;
	writeln(`
			<!doctype html>
			<html lang="rl"> <!-- RogueLike! -->
			<head>
			<meta charset="utf-8">
			<title>SmuglerRL HTML dump!</title>
			</head>
			<body fgcolor="#ffffff" bgcolor="#000000">
			<div style="font-family: monospace">`);

	bool clr_changed, bgclr_changed;
	RGBColour fg, last_fg, bg, last_bg;
	foreach (row; map.get_tiles) {
		foreach (tile; row) {
			fg = tile.fgcolour;
			bg = tile.bgcolour;

			if (fg != last_fg) {
				clr_changed = true;
			} else {
				clr_changed = false;
			}
			if (bg != last_bg) {
				bgclr_changed = true;
			} else {
				bgclr_changed = false;
			}

			if (clr_changed) write(`</font>`);
			if (bgclr_changed) write(`</font>`);
			if (clr_changed) {
				with (fg) writef(`<font color="#%02x%02x%02x">`, r, g, b);
				last_fg = fg;
			}
			if (bgclr_changed) {
				with (bg) writef(`<font bgcolor="#%02x%02x%02x">`, r, g, b);
				last_bg = bg;
			}
			write(cast(dchar)tile.glyph);
		}
		writeln("<br>");
	}
	writeln(`</div>
		</body>
		</html>`);
}
+/
