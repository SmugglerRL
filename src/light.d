import stdlib;
import map;
import colour;
import being;
import constants;

float sqavg(float[] nums) {
	float sum;
	foreach (i; nums) {
		sum += i * i;
	}

	return pow(sum, 1. / nums.length);
}
T avg(T)(T[] nums) {
	T sum;
	foreach (i; nums) {
		sum += i;
	}
	return sum / nums.length;
}

void do_colour(ref Map map, list!Being mons) {
	import std.stdio;
	struct Lightcell {
		float value = 0;
		uint r, g, b;
		uint nums;
	}
	Lightcell[][] base = new Lightcell[][](map_x, map_y);
	const Lightmap umap = mons.front.lightmap;

	foreach (mon; mons) {
		if (!mon.light_source) {
			continue;
		}

		auto loc = Vector2n(mon.lightmap.top_edge, mon.lightmap.left_edge);
		foreach (lightrow; mon.lightmap.get_lights) {
			foreach (light; lightrow) {
				if (light.lit && umap[loc].visible) {
					base[loc.y][loc.x].value += mon.lightmap[loc].light;
					base[loc.y][loc.x].nums++;
					base[loc.y][loc.x].r += mon.lightclr.r;
					base[loc.y][loc.x].g += mon.lightclr.g;
					base[loc.y][loc.x].b += mon.lightclr.b;
				}

				loc.x++;
			}
			loc.y++;
			loc.x -= 1 + 2*mon.fov_dist;
		}
	}

	foreach (y; 0 .. map_y) {
		foreach (x; 0 .. map_x) {
			if (base[y][x].nums == 0) {
				map[y, x].bgcolour = RGBColour(0, 0, 0);
				continue;
			}

			float f1 = pow(base[y][x].value, .8) / 4.3;
			float f = f1 / (f1+1); //smaxf;
//			map[y, x].bgcolour = RGBColour(pow(f/2, 1.5), pow(f/1.4, 3), pow(f/16, 2));
			//map[y, x].bgcolour = RGBColour(f*1.1, f, f/9);
			with (base[y][x]) map[y, x].bgcolour = RGBColour(cast(ubyte)(f*r/nums), cast(ubyte)(f*g/nums), cast(ubyte)(f*b/nums)); //f*r/nums, f*g/nums, f*b/nums);

			//writefln("Set bg to %s", map[y, x].bgcolour);
		}
	}
}
