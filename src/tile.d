import glyph;
import colour;

struct Tilebase {
	Glyph glyph;
	bool blocks_light, walkable;
	RGBColour fgcolour;
	private struct _attr {bool reverse, italic, bold, underline; }
	_attr attrs;
}

struct Tile {
	Tilebase base;
	alias base this;

	RGBColour bgcolour;
}

enum Tiles: Tilebase {
	floor = Tilebase(Glyph.middot, false, true, RGBColour(0x4c4c4c)),
	rock = Tilebase(Glyph.dark_square, true, false, RGBColour(0x663300)),
}
