public:
enum bool hascent = false;

enum int min_x = 80, min_y = 25, map_x = 1000, map_y = 1000;

// Actions are done by Beings
enum Action {
	None,
	MoveNorth, MoveSouth, MoveLeft, MoveRight, MoveNorthLeft, MoveNorthRight, MoveSouthLeft, MoveSouthRight,
	AttackNorth, AttackSouth, AttackLeft, AttackRight, AttackNorthLeft, AttackNorthRight, AttackSouthLeft, AttackSouthRight,
	Wait,
	Printloc,
	Pickup,
	Showinv,
	Quit
}
