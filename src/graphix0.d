import colour;
import logging;
import util;
import constants;


interface CharGfx0 {
	void refresh();
	uint maxx();
	uint maxy();
	dchar getch();
	void settitle(string title);
	void mvaddch(dchar ch, uint y, uint x, RGBColour fg = RGBColour(0xffffff), RGBColour bg = RGBColour(0x000000), bool bold = false, bool italic = false, bool underline = false, bool reverse = false);
	void close();

	final void printext(string text, uint y, uint x, RGBColour fg = RGBColour(0xffffff), RGBColour bg = RGBColour(0x000000), bool bold = false, bool italic = false, bool underline = false, bool reverse = false) {
		import std.conv: dtext;
		foreach (ch; dtext(text)) {
			mvaddch(ch, y, x++ /* no wrapping, just go off the edge of the screen */, fg, bg, bold, italic, underline, reverse);
		}
	}
}


class SDL0: CharGfx0 {
	import derelict.sdl2.image, derelict.sdl2.sdl, derelict.sdl2.ttf, derelict.sdl2.mixer;

	SDL_Renderer *renderer;
	SDL_Window *sdl_window;

	SDL_Texture*[] tileset;
	SDL_Texture*[] tileset_italic;
	SDL_Texture*[] tileset_italicbold;
	SDL_Texture*[] tileset_bold;

	private struct CoordChar {
		dchar ch = ' ';
		RGBColour fg = RGBColour(0xffffff), bg = RGBColour(0x0);
		bool bold, italic, underline, reverse = false;
	}

	CoordChar[][] screen;

	enum tile_width = 12;
	enum tile_height = tile_width * 2;

	private void sdlerror() {
		import std.string: fromStringz;
		throw new Exception(cast(string)("Error from SDL.  SDL says: " ~ fromStringz(SDL_GetError())));
	}


	private bool loadfont(string fpath, ushort height, ref SDL_Texture*[] target) {
		import std.string: toStringz;

		TTF_Font *font = TTF_OpenFont(toStringz(fpath), height);

		SDL_Surface *surface;
		SDL_Color white = SDL_Color(255, 255, 255, 0);

		if (!font) {
			return false;
		}

		TTF_SetFontKerning(font, 0);

		ushort[2] text;

		target = new SDL_Texture*[65536];

		foreach (ushort i; 0 .. 65536) {
			if (TTF_GlyphIsProvided(font, i)) {
				text[0] = i;
				surface = TTF_RenderUNICODE_Blended(font, text.ptr, white);
				target[i] = SDL_CreateTextureFromSurface(renderer, surface);
				SDL_FreeSurface(surface);
			} else {
				target[i] = null;
			}

		}

		TTF_CloseFont(font);

		return true;
	}


	this() {
		version (dynamic_sdl2) {
			DerelictSDL2.load();
			DerelictSDL2Image.load();
			DerelictSDL2TTF.load();
			DerelictSDL2Mixer.load();
		}

		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO) < 0) {
			sdlerror();
		}

		if (TTF_Init() == -1) {
			sdlerror();
		}

		/*
		{
			int mix_flags = MIX_INIT_FLAC | MIX_INIT_MP3 | MIX_INIT_OGG;

			if ((Mix_Init(mix_flags) & mix_flags) != mix_flags)
				sdlerror();
		}
		// thanks lazy foo
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
			sdlerror();
		}
		// no thanks for the formatting, though, you lazy foo!
		*/


		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");

		// Title, bunch of stuff, resolution, borderless fullscreen
		// SDL_WINDOW_FULLSCREEN actually changes the video mode
		sdl_window = SDL_CreateWindow(null, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, min_x * tile_width, min_y * tile_height, cast(SDL_WindowFlags)0);
		if (!sdl_window) { sdlerror(); }
		//      sdl_window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0, SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP);
		renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

		if (!loadfont("assets/font/dvsm.ttf", tile_height, tileset)) { sdlerror(); }
		if (!loadfont("assets/font/dvsm-italic.ttf", tile_height, tileset_italic)) { sdlerror(); }
		if (!loadfont("assets/font/dvsm-bolditalic.ttf", tile_height, tileset_italicbold)) { sdlerror(); }
		if (!loadfont("assets/font/dvsm-bold.ttf", tile_height, tileset_bold)) { sdlerror(); }

		screen = new CoordChar[][](maxx(), maxy());
	}

	void refresh() {
		// clear the screen
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		// draw the characters onto it
		foreach (y; 0 .. maxy()) {
			foreach (x; 0 .. maxx()) {
				actual_mvaddch(screen[y][x], y, x);
			}
		}


		// Blit it to the screen.  No magic happening here, just some x11 (or win32 or cocoa or what-have-you) calls
		SDL_RenderPresent(renderer);
	}

	uint maxx() {
		int w;
		SDL_GetWindowSize(sdl_window, &w, null);
		return w / tile_width;
	}

	uint maxy() {
		int h;
		SDL_GetWindowSize(sdl_window, null, &h);
		return h / tile_height;
	}

	dchar getch() {
		import std.string: fromStringz;

		SDL_Event e;

		SDL_StartTextInput();
		while (true) {
			if (SDL_WaitEvent(&e) == 1) {
				if (e.type == SDL_TEXTINPUT) {
					//return to!dchar(e.text.text);
					return e.text.text[0];
				// gained focus, so we need to redraw.  IDK why
				} else if (e.type == SDL_WINDOWEVENT) {
					refresh();
				}
			} else {
				sdlerror();
			}
		}
	}

	void settitle(string title) {
		import std.string: toStringz;
		SDL_SetWindowTitle(sdl_window, toStringz(title));
	}

	void mvaddch(dchar ch, uint y, uint x, RGBColour fg = RGBColour(0xffffff), RGBColour bg = RGBColour(0x000000), bool bold = false, bool italic = false, bool underline = false, bool reverse = false) {
		if (y >= screen.length) {
			screen.length = maxy();
		}
		foreach (ref col; screen) {
			col.length = maxx();
		}

		screen[y][x] = CoordChar(ch, fg, bg, bold, italic, underline, reverse);
	}

	private void actual_mvaddch(CoordChar character, uint y, uint x) { with (character) {
		if (reverse) {
			RGBColour tmp = fg;
			fg = bg;
			bg = tmp;
		}

		SDL_Texture *renderedchar;

		if (ch > wchar.max) {
			log("Character %s large, must fit into a wchar (this is an SDL limitation)", ch);
			ch = '?';
		}

		/*
		SDL_Texture*[][][] tilesetset = [[tileset, tileset_italic], [tileset_bold, tileset_italicbold]];
		SDL_Texture*[] tiles = tilesetset[bold][italic];
		*/
		SDL_Texture*[] tiles;
		if (bold) {
			if (italic) {
				tiles = tileset_italicbold;
			} else {
				tiles = tileset_bold;
			}
		} else if (italic) {
			tiles = tileset_italic;
		} else {
			tiles = tileset;
		}

		// NULL means there's no glyph so fall back to '?'
		if (tiles[ch] is null) {
			renderedchar = tiles['?'];
		} else {
			renderedchar = tiles[ch];
		}

		// We just draw a square with colour bg, then draw the char on top of
		// it, but with transparency so we see the bg.
		SDL_Rect tile;


		tile.y = y * tile_height;
		tile.x = x * tile_width;
		tile.w = tile_width;
		tile.h = tile_height;

		// Set the colour to draw with
		SDL_SetRenderDrawColor(renderer, bg.r, bg.g, bg.b, 255);

		// Actually draw it
		SDL_RenderFillRect(renderer, &tile);

		// Colourize the letter itself.  The parts that aren't the letter will
		// also get colourized, but that doesn't matter because they have alpha
		// 256
		SDL_SetTextureColorMod(renderedchar, fg.r, fg.g, fg.b);

		// And finally, copy everything over to the actual renderer
		SDL_RenderCopy(renderer, renderedchar, null, &tile);
	}}

	void close() {
		foreach (tilesets; [tileset, tileset_bold, tileset_italic, tileset_italicbold]) {
			foreach (ref texture; tilesets) {
				SDL_DestroyTexture(texture);
			}
		}

		SDL_DestroyRenderer(renderer);
		SDL_DestroyWindow(sdl_window);
		SDL_QuitSubSystem(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO);
		SDL_Quit();
	}
}




class NCurses0: CharGfx0 {
	import deimos.ncurses;
	this() {
		import core.stdc.locale: setlocale, LC_ALL;

		setlocale(LC_ALL, "en_US.UTF-8");
		initscr();

		clear();
		noecho();
		cbreak();
		keypad(stdscr, true);
		curs_set(0);
		start_color();
	}
	void close() { endwin(); }

	void refresh() {
		static import deimos.ncurses;
		deimos.ncurses.refresh();
	}

	dchar getch() {
		static import deimos.ncurses;
		return deimos.ncurses.getch();
	}

	uint maxx() { return COLS; }
	uint maxy() { return LINES; }
	void settitle(string title) {}

	void mvaddch(dchar ch, uint y, uint x, RGBColour fg, RGBColour bg, bool bold, bool italic, bool underline, bool reverse) {
		import std.conv: text;
		import std.string: toStringz;

		static ushort[ubyte[2]] clrmap;
		static ushort maxclrindex;

		ubyte[2] clrs = [get256colour(fg),  get256colour(bg)];
		if (clrs !in clrmap) {
			init_pair(maxclrindex, clrs[0], clrs[1]);
			clrmap[clrs] = maxclrindex++;
		}

		uint attrs() {
			return (bold ? A_BOLD : 0) | /*(italic ? A_ITALIC : 0) | */ (underline ? A_UNDERLINE : 0) | (reverse ? A_REVERSE : 0);
		}

		attron(attrs() | COLOR_PAIR(*(clrs in clrmap)));

		mvprintw(y, x, toStringz(text(ch))); // TODO

		attroff(attrs() | COLOR_PAIR(*(clrs in clrmap)));
	}
}




class BLT0: CharGfx0 {
	import BearLibTerminal;
	this() {
		terminal.open();
		terminal.set("input.cursor-blink-rate=2147483647");
		terminal.set("input.cursor-symbol=0x2588");

		terminal.set("font: assets/font/dvsm.ttf, size=12");
		terminal.set("default font: assets/font/dvsm.ttf, size=12");
		terminal.set("italic font: assets/font/dvsm-italic.ttf, size=12");
		terminal.set("bold font: assets/font/dvsm-bold.ttf, size=12");
		terminal.set("bolditalic font: assets/font/dvsm-bolditalic.ttf, size=12");

	//	terminal.set("font: use-box-drawing=false, use-block-elements=false");
		terminal.set("window.resizeable=true");
	}
	void close() { terminal.close(); }

	void refresh() { terminal.refresh(); }

	uint maxx() { return terminal.state(terminal.keycode.width); }
	uint maxy() { return terminal.state(terminal.keycode.height); }

	void settitle(string title) {
		terminal.setf("window.title=%s", title);
	}

	void mvaddch(dchar ch, uint y, uint x, RGBColour fg, RGBColour bg, bool bold, bool italic, bool underline, bool reverse) {
		if (reverse) {
			RGBColour tmp = fg;
			fg = bg;
			bg = tmp;
		}
		terminal.colour(fg);
		terminal.bkcolour(bg);


		// it's not beautiful, and it's unclear there even exists an
		// idiomatic way.  But this is the most performant way, and this
		// is a function that gets called thousands of times per refresh
		static immutable string[2][2] fontab = [["default", "italic"], ["bold", "bolditalic"]];
		terminal.font(fontab[bold][italic]);

		// TODO implement bold and italic (have to load alternate fonts)
		terminal.put(x, y, ch);

		terminal.layer(1);
		if (underline) {
			terminal.put(x, y, '▁');
		} else {
			terminal.put(x, y, ' ');
		}
		terminal.layer(0);

		terminal.font("default");
	}


	dchar getch() {
		dchar[terminal.keycode] keycode2char = [terminal.keycode.a: 'a',
			terminal.keycode.b: 'b',
			terminal.keycode.c: 'c',
			terminal.keycode.d: 'd',
			terminal.keycode.e: 'e',
			terminal.keycode.f: 'f',
			terminal.keycode.g: 'g',
			terminal.keycode.h: 'h',
			terminal.keycode.i: 'i',
			terminal.keycode.j: 'j',
			terminal.keycode.k: 'k',
			terminal.keycode.l: 'l',
			terminal.keycode.m: 'm',
			terminal.keycode.n: 'n',
			terminal.keycode.o: 'o',
			terminal.keycode.p: 'p',
			terminal.keycode.q: 'q',
			terminal.keycode.r: 'r',
			terminal.keycode.s: 's',
			terminal.keycode.t: 't',
			terminal.keycode.u: 'u',
			terminal.keycode.v: 'v',
			terminal.keycode.w: 'w',
			terminal.keycode.x: 'x',
			terminal.keycode.y: 'y',
			terminal.keycode.z: 'z',
			terminal.keycode.KP_1: '1',
			terminal.keycode.KP_2: '2',
			terminal.keycode.KP_3: '3',
			terminal.keycode.KP_4: '4',
			terminal.keycode.KP_5: '5',
			terminal.keycode.KP_6: '6',
			terminal.keycode.KP_7: '7',
			terminal.keycode.KP_8: '8',
			terminal.keycode.KP_9: '9',
			terminal.keycode.KP_0: '0',
			terminal.keycode.enter: '\n',
			terminal.keycode.escape: '\033',
			terminal.keycode.backspace: '\b',
			terminal.keycode.tab: '\t',
			terminal.keycode.space: ' ',
			terminal.keycode.minus: '-',
			terminal.keycode.equals: '=',
			terminal.keycode.lbracket: '[',
			terminal.keycode.rbracket: ']',
			terminal.keycode.backslash: '\\',
			terminal.keycode.semicolon: ';',
			terminal.keycode.apostrophe: '\'',
			terminal.keycode.grave: '`',
			terminal.keycode.comma: ',',
			terminal.keycode.period: '.',
			terminal.keycode.slash: '/',
			/+F1 =
				F2 =
				F3 =
				F4 =
				F5 =
				F6 =
				F7 =
				F8 =
				F9 =
				F10 =
				F11 =
				F12 =
				pause = 0x48 /* Pause/Break */,
			insert = 0x49,
			home = 0x4a,
			pageup = 0x4b,
			K_delete = 0x4c,
			end = 0x4d,
			pagedown = 0x4e,
			right = 0x4F /* Right arrow */,
			left = 0x50 /* Left arrow */,
			down = 0x51 /* Down arrow */,
			up = 0x52 /* Up arrow */,
			+/
				terminal.keycode.KP_divide: '/',
			terminal.keycode.KP_multiply: '*',
			terminal.keycode.KP_minus: '-',
			terminal.keycode.KP_plus: '+',
			terminal.keycode.KP_enter: '\n',
			terminal.keycode.KP_1: '1',
			terminal.keycode.KP_2: '2',
			terminal.keycode.KP_3: '3',
			terminal.keycode.KP_4: '4',
			terminal.keycode.KP_5: '5',
			terminal.keycode.KP_6: '6',
			terminal.keycode.KP_7: '7',
			terminal.keycode.KP_8: '8',
			terminal.keycode.KP_9: '9',
			terminal.keycode.KP_0: '0',
			terminal.keycode.KP_period: '.',

			/+shift = 0x70,
			control = 0x71,
			alt = 0x72,

			mouse_left = 0x80 /* Buttons */,
			mouse_right = 0x81,
			mouse_middle = 0x82,
			mouse_x1 = 0x83,
			mouse_x2 = 0x84,
			mouse_move = 0x85 /* Movement event */,
			mouse_scroll = 0x86 /* Mouse scroll event */,
			mouse_x = 0x87 /* Cusor position in cells */,
			mouse_y = 0x88,
			mouse_pixel_x = 0x89 /* Cursor position in pixels */,
			mouse_pixel_y = 0x8A,
			mouse_wheel = 0x8B /* Scroll direction and amount */,
			mouse_clicks = 0x8C /* Number of consecutive clicks */,
			+/
				];
		terminal.keycode k;
		while ((k = terminal.read()) !in keycode2char) {}
		return keycode2char[k];
	}
}
