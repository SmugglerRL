public import std.random: rnd = uniform;
import util;

// generates a number between 1 and number, inclusive
T rn1(T)(T number) {
	return rnd(1, number+1);
}

// generates a number between 0 and number-1, inclusive
T rn2(T)(T number) {
	return rnd(0, number);
}

// generates numbers geometrically.  Increasingly likely to generate lower numbers
T rne(T)(T x) {
	T tmp;

	//	utmp = (u.ulevel < 15) ? 5 : u.ulevel / 3;
	//	was ^^ in nethack
	while (tmp < 100 && !rn2(x)) {
		tmp++;
	}

	return tmp;
}

// rnz().  Here be dragons.
T rnz(T)(T i) {
	T x = i;
	T tmp = 1000;

	tmp += rn2(1000);
	tmp *= rne(4);

	if (rn2(2)) {
		x *= tmp;
		x /= 1000;
	} else {
		x *= 1000;
		x /= tmp;
	}
	return x;
}

// for dnd style Ndn stuff like 5d6 will generate a number between 1 and 6 5
// times and return the cumulative answer
// actual code is from nethack
T d(T)(T n, T x) {
	T tmp = 0;

	while (n--)
		tmp += rn1(x);
	return tmp;
}

// a/b chance of returning true
bool chances(uint a, uint b) {
	//if (a > b) return chances(b, a);
	// mod 100 -- 123% = 23%
	// e.g., 8/5 = 3/5 + 5/5 = 3/5 + 100%.  -100%.  = 3/5
	//if (a > b) return chances(a%b, b);
	//if (a > b) throw new InternalError("HELP ME I DON'T KNOW HOW TO ADD SHIT
	//if (a >= b) return true; // boring as it is, this is probably the sane choice
	if (a > b) { // actually fuck sanity let's SEGFAULT
		bool *nullptr = null;
		ubyte offset;

		if (true) {
			nullptr = cast(bool*)0x7;
			offset = 0x9;
		} else {
			nullptr = cast(bool*)0x9;
			offset = 0x7;
		}
		nullptr -= offset;

		if (nullptr < cast(bool*)0) {
			nullptr += 2;
		} else if (nullptr > cast(bool*)0) {
			nullptr -= 2;
		}

		return *(cast(bool*)nullptr);
	}
	//if (a > b) return chances(a, b); // dmd doesn't like derefing null, but stack overflow is almost as good


	return (a/b)*uint.max > rnd!uint();
}
