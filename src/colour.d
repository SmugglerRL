import stdlib;
import util;


// Many thanks, FIQ
bool isvalidcolour(string hexcolour) {
	char nam;
	ubyte index, count;

	if (hexcolour[0] == '#') {
		index++;
	}

	for (;index < hexcolour.length; index++) {
		nam = hexcolour[index];
		if (!nam) {
			break;
		}

		if (!((('0' <= nam) && (nam <= '9')) || (('A' <= nam) && (nam <= 'F')))) {
			return false;
		}

		count++;
		if (count > 6) {
			return false;
		}
	}

	return count == 6;
}

static if(0) {
	bool isvalidcolour(string hexcolour) {
		import std.regex: ctRegex, matchFirst;
		auto hexcolourmatch = ctRegex!("#?[0-9a-fA-F]{3,6}");

		if (matchFirst(hexcolour, hexcolourmatch).empty) {
			return false;
		} else {
			return true;
		}
	}
}

struct RGBColour {
	ubyte r=0xFF, g=0xFF, b=0xFF;
	pragma(inline, true) pure this(ubyte in_r, ubyte in_g, ubyte in_b) {
		r = in_r;
		g = in_g;
		b = in_b;
	}


	// No constructor for colours of the form 0xFFF, because there's no way to tell the difference between 0xFFF and 0x000FFF
	pragma(inline, true) pure this(uint clr) {
		b = clr % 256;

		/* An interesting optimization.  Ordinarily, you do b = clr % 256,
		 * g = (clr / 256) % 256, r = (clr / 256 / 256) % 256.  left/right
		 * shifts are cheaper than multiplication, and by reassigning clr,
		 * there's an extra multiplication step that doesn't occur.
		 */
		g = (clr >>= 8) % 256;
		r = (clr >> 8) % 256;
	}
	/*
	pragma(inline, true) pure this(float in_r, float in_g, float in_b) {
		r = cast(ubyte)(in_r * 255.8);
		g = cast(ubyte)(in_g * 255.8);
		b = cast(ubyte)(in_b * 255.8);
	}
	*/

	pure this(string hexcolour, bool hasvalidated=false) {
		// strip leading #, if present
		if ((hexcolour.length == 7) || (hexcolour.length == 4)) {
			hexcolour = hexcolour[1..$];
		}

		if (hasvalidated) {
			if (hexcolour.length == 6) {
				r = hexcolour[0..2].to!ubyte(16);
				g = hexcolour[2..4].to!ubyte(16);
				b = hexcolour[4..6].to!ubyte(16);
			} else {
				// The ranges here are to get a string just one character long so to!ubyte works
				r = cast(ubyte)(hexcolour[0..1].to!ubyte(16)*16);
				g = cast(ubyte)(hexcolour[1..2].to!ubyte(16)*16);
				b = cast(ubyte)(hexcolour[2..3].to!ubyte(16)*16);
			}
		} else {
			throw new InternalError("RGBColour constructor called but rgbcolour not validated!");
		}
	}

        // shamelessly stolen from https://github.com/adamdruppe/arsd/blob/master/color.d#L448
        // HSL conersion
	pure this(HSLColour hsl) {
		nothrow @safe @nogc pure real absInternal(real a) { return a < 0 ? -a : a; }
		real h = hsl.h, s = hsl.s, l = hsl.l;

		h = h % 360;

		real C = (1 - absInternal(2 * l - 1)) * s;

		real hPrime = h / 60;

		real X = C * (1 - absInternal(hPrime % 2 - 1));

		real tmpr, tmpg, tmpb;

		if (h is real.nan) {
			tmpr = tmpg = tmpb = 0;
		} else if (hPrime >= 0 && hPrime < 1) {
			tmpr = C;
			tmpg = X;
			tmpb = 0;
		} else if (hPrime >= 1 && hPrime < 2) {
			tmpr = X;
			tmpg = C;
			tmpb = 0;
		} else if (hPrime >= 2 && hPrime < 3) {
			tmpr = 0;
			tmpg = C;
			tmpb = X;
		} else if (hPrime >= 3 && hPrime < 4) {
			tmpr = 0;
			tmpg = X;
			tmpb = C;
		} else if (hPrime >= 4 && hPrime < 5) {
			tmpr = X;
			tmpg = 0;
			tmpb = C;
		} else if (hPrime >= 5 && hPrime < 6) {
			tmpr = C;
			tmpg = 0;
			tmpb = X;
		}

		real m = l - C / 2;

		tmpr += m;
		tmpg += m;
		tmpb += m;

		r = cast(ubyte)(tmpr * 255);
		g = cast(ubyte)(tmpg * 255);
		b = cast(ubyte)(tmpb * 255);
	}

	pragma(inline, true) this(string hexcolour) {
		if (!isvalidcolour(hexcolour)) {
			throw new InternalError("RGBColour constructor called with faulty colour.");
		}
		this(hexcolour, true);
	}

	pragma(inline, true) pure uint toint() {
		return (255 << 24) | (r << 16) | (g << 8) | b;
	}
	alias toint this;

	RGBColour darken(ubyte level) {
		RGBColour tmp;
		tmp.r = cast(ubyte) ((r < level) ? 0 : r-level);
		tmp.g = cast(ubyte) ((g < level) ? 0 : g-level);
		tmp.b = cast(ubyte) ((b < level) ? 0 : b-level);

		return tmp;
	}

	RGBColour lighten(ubyte level) {
		RGBColour tmp;

		tmp.r = cast(ubyte) (((255-r) < level) ? 255 : r+level);
		tmp.g = cast(ubyte) (((255-g) < level) ? 255 : g+level);
		tmp.b = cast(ubyte) (((255-b) < level) ? 255 : b+level);

		return tmp;
	}
}

struct HSLColour {
	union {
		struct {
			real h, s, l;
		}
		real[3] colours;
	}

	private import std.traits: isFloatingPoint;
	pure this(T)(T h, T s, T l) if (isFloatingPoint!T) {
		this.h = cast(real)h;
		this.s = cast(real)s;
		this.l = cast(real)l;
	}

	// stolen from https://github.com/adamdruppe/arsd/blob/master/color.d#L501
	pure this(RGBColour rgb) {
		pragma(inline, true) {
			real maxInternal(real a, real b, real c) {
				auto m = a;
				if (b > m) m = b;
				if (c > m) m = c;
				return m;
			}
			real minInternal(real a, real b, real c) {
				real m = a;
				if (b < m) m = b;
				if (c < m) m = c;
				return m;
			}
		}

		real r1 = cast(real) rgb.r / 255.0;
		real g1 = cast(real) rgb.g / 255.0;
		real b1 = cast(real) rgb.b / 255.0;

		real maxColor = maxInternal(r1, g1, b1);
		real minColor = minInternal(r1, g1, b1);

		real tmpl = (maxColor + minColor) / 2;
		real tmps = 0;
		real tmph = 0;

		if (maxColor != minColor) {
			if(tmpl < 0.5) {
				tmps = (maxColor - minColor) / (maxColor + minColor);
			} else {
				tmps = (maxColor - minColor) / (2.0 - maxColor - minColor);
			}
			if(r1 == maxColor) {
				tmph = (g1-b1) / (maxColor - minColor);
			} else if(g1 == maxColor) {
				tmph = 2.0 + (b1 - r1) / (maxColor - minColor);
			} else {
				tmph = 4.0 + (r1 - g1) / (maxColor - minColor);
			}
		}

		tmph = tmph * 60;
		if(tmph < 0){
			tmph += 360;
		}

		h = tmph;
		s = tmps;
		l = tmpl;
	}
}
