// useful public d std imports
// also internal std import

public:
// internal std:
import logging; // technically 

// d std:
import std.random: choice;
import std.conv: to;
import std.math: sqrt;
import std.container.rbtree: set = redBlackTree;
import std.string: format;
//import std.algorithm.iteration: split = splitter;
import std.array: join, split;
import std.container: list = DList;
import std.algorithm.comparison: min, max;
import std.math: abs, log, pow;

pragma(inline, true) float sqrt(int num) {
	return sqrt(cast(float)num);
}
