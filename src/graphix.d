import constants;
import util;
import graphix0;
import map;
import stdlib;
import colour;
import being;
import item;
import logging;

interface Graphics1 {
	void refresh(uint uy, uint ux, list!Being mons, list!Item items, Map map);
	// Originally, this was pragma(inline, true) pure final void pline(T...)(string s, T args)
	// But the opportunity was too good to pass up
	// then it was
	//pragma(inline, true) pure const static inout shared ref nothrow override @property @nogc @safe public final void
	// in all its glory.  But after about a year, I found out that this didn't actually work, which brings you to the state you see today :<<
	pragma(inline, true) public final void pline(T...)(T args) {
		if (args.length == 1) {
			this.pline(to!string(args[0]));
		} else {
			this.pline(format(args));
		}
	}
	void pline(string s);
	int maxx();
	int maxy();
	dchar getch();
	void close();
}

private struct Lens { int startx, endx, cursx, starty, endy, cursy; }

// Find the x-y coordinates to start drawing from in the map so the camera is centred on the player
// FUN FACT while coding this, I had no idea what it did or how
// it worked.  I still have no idea how it works.  But it does!
private @nogc @safe final Lens focuscamera(int width, int height, int y, int x) {
	import std.math: round;
	Lens tmp;
	int startx, starty, endx, endy;
	int offsetx, offsety; // offsets are distance from top edge, left edge to centre
	int cursx, cursy;
	height -= 1;

	offsetx = cast(int)round(width / 2.0);
	offsety = cast(int)round(height / 2.0);

	startx = x - offsetx;
	cursx = offsetx;
	starty = y - offsety;
	cursy = offsety;

	endx = startx + width;
	endy = starty + height;

	tmp.startx = startx;
	tmp.starty = starty;
	tmp.endx = endx;
	tmp.endy = endy;
	tmp.cursx = cursx;
	tmp.cursy = cursy;

	return tmp;
}

class Default_chargfx(G0type: CharGfx0): Graphics1 {
	CharGfx0 graphics0;
	bool msgdrawn;
	list!string msg_queue;

	this(string title="") {
		this.graphics0 = new G0type();

		graphics0.settitle(title);
	}
	void close() { graphics0.close(); }

	void drawmons(Lens lens, list!Being mons, Map map) {
		const Lightmap umap = mons.front.lightmap;
		foreach (mon; mons) {
			if (!umap[mon.loc].visible) {
				continue;
			}

			// so this is a bit confusing.  We're testing if it's not in range and if so continuing
			if (!(((lens.starty <= cast(int)mon.loc.y) && (cast(int)mon.loc.y < lens.endy))
			    && (lens.startx <= cast(int)mon.loc.x) && (cast(int)mon.loc.x < lens.endx))) {
				continue;
			}

			/* By default, we're just drawing at (y, x).  But we
			 * need to draw one place lower than normal to make room
			 * for the message bar.
			 */
			with (mon.attrs) {
				graphics0.mvaddch(mon.glyph, mon.loc.y - lens.starty + 1, mon.loc.x - lens.startx, mon.fgcolour, mon.bgcolour, bold, italic, underline, reverse);
			}
		}
	}
	void drawitems(Lens lens, list!Item items, const ref Lightmap umap) {
		foreach (const ref item; items) {
			if (!umap[item.loc].visible) {
				continue;
			}

			// so this is a bit confusing.  We're testing if it's not in range and if so continuing
			if (!(((lens.starty <= cast(int)item.loc.y) && (cast(int)item.loc.y < lens.endy))
			    && (lens.startx <= cast(int)item.loc.x) && (cast(int)item.loc.x < lens.endx))) {
				continue;
			}

			graphics0.mvaddch(item.glyph, item.loc.y - lens.starty + 1, item.loc.x - lens.startx, RGBColour(0xffffff), RGBColour(0xff0000), false, false, false, false);
		}
	}

	void drawmap(Lens lens, Map map, const ref Lightmap lights) {
		foreach (y; lens.starty..lens.endy) {
			foreach (x; lens.startx..lens.endx) {
				Vector2n maploc = Vector2n(y, x);

				/* x and y both start at 1, so we want to get them to 0
				 * in order to align them with the edges of the terminal.
				 * But it's okay to "add" 1 to y, because we want to leave
				 * an extra line up to for messages.
				 */
				with (map[maploc].attrs)
					graphics0.mvaddch(map[maploc].glyph, y - lens.starty + 1, x - lens.startx, lights[maploc].visible ? map[maploc].fgcolour.lighten(20) : map[maploc].fgcolour.darken(20), map[maploc].bgcolour, bold, italic, underline, reverse);
			}
		}

	}

	void clearmsgbar() {
		graphics0.printext(fillstr(maxx()), 0, 0);
	}

	private static immutable more = " --More--";

	void pline(string msg) {
		msg_queue.insertBack(msg);
	}


	// true => keep printing, false => stop sending messages
	private bool actual_pline(string[] messagewords) {
		if (messagewords.length == 1) {
			clearmsgbar();
			graphics0.printext(messagewords[0], 0, 0);
			graphics0.refresh();
		} else {
			loop: foreach (lineindex; 0 .. messagewords.length) {
				graphics0.printext(messagewords[lineindex], 0, 0);
				graphics0.refresh();
				if (lineindex < messagewords.length-1) {
					dchar c;
					do {
						c = graphics0.getch();
						if (c == '\033') {
							return false;
						}
					} while ((c != '\n') && (c != ' '));

					clearmsgbar();
				}
			}
		}

		return true;
	}

	private void fire_plines() {
		if (msg_queue.empty) {
			return;
		}

		pure string[] chopupmsg(string msgtext, size_t width, bool have_more /* there's another message after*/) {
			string[] words = msgtext.split;

			size_t tmp;
			string[] ret;
			string str;
			immutable size_t maxlen = width - more.length;

			while (words.length) {
				str ~= words[0];
				words = words[1 .. $];

				if (!words.length) {
					ret ~= str;
					break;
				}

				if (str.length + words[0].length > maxlen) {
					str ~= more;
					ret ~= str;
					str = null;
				} else {
					str ~= ' ';
				}
			}

			if (have_more) {
				ret[$-1] ~= more;
			}

			return ret;
		}

		string[][] messages;

		string[] last_msg = chopupmsg(msg_queue.back, maxx(), false);
		msg_queue.removeBack();
		foreach (i; msg_queue) {
			messages ~= chopupmsg(i, maxx(), true);
		}
		messages ~= last_msg;
		msg_queue.clear();

oloop:		foreach (msg; messages[0 .. $-1]) {
			if (!actual_pline(msg)) {
				goto lastmsg;
			}

			while (true) {
				dchar c = graphics0.getch();
				if ((c == '\n') || (c == ' ')) {
					continue oloop;
				} else if (c == '\033') {
					goto lastmsg;
				}
			}
		}
		if (!actual_pline(messages[$-1])) {
			goto lastmsg;
		}
		return;

lastmsg:
		clearmsgbar();
		graphics0.printext(messages[$-1][$-1], 0, 0);
		graphics0.refresh();
	}

	int maxx() { return graphics0.maxx(); }
	int maxy() { return graphics0.maxy(); }
	void refresh(uint uy, uint ux, list!Being mons, list!Item items, Map map) {
		auto lens = focuscamera(maxx(), maxy(), uy, ux);

		drawmap(lens, map, mons.front.lightmap);
		drawitems(lens, items, mons.front.lightmap);
		drawmons(lens, mons, map);

		graphics0.refresh();

		fire_plines(); // calls its own refresh
	}

	dchar getch() { return graphics0.getch; }
}
