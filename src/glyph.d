enum Glyph: dchar {
	space = ' ',
	a = 'a', b = 'b', c = 'c', d = 'd', e = 'e', f = 'f', g = 'g', h = 'h', i = 'i', j = 'j', k = 'k', l = 'l', m = 'm', n = 'n', o = 'o', p = 'p', q = 'q', r = 'r', s = 's', t = 't', u = 'u', v = 'v', w = 'w', x = 'x', y = 'y', z = 'z', A = 'A', B = 'B', C = 'C', D = 'D', E = 'E', F = 'F', G = 'G', H = 'H', I = 'I', J = 'J', K = 'K', L = 'L', M = 'M', N = 'N', O = 'O', P = 'P', Q = 'Q', R = 'R', S = 'S', T = 'T', U = 'U', V = 'V', W = 'W', X = 'X', Y = 'Y', Z = 'Z',
	hash = '#',
	middot = '·',
	at = '@',
	paren_left = '(', paren_right = ')',
	brace_left = '{', brace_right = '}',
	bracket_left = '[', bracket_right = ']',

	light_square =	'░',	/* they have gradients of how bright they are, but that's
				 * handled elsewhere */
	medium_square =	'▒',
	dark_square =	'▓',
	full_square =	'█',

	/* It turns out that there are a *lot* of different box drawing
	 * and we want to utilize them all to show, say, partially dug through
	 * walls.  The price is this huge enum */
	wall_horizontal_thick =			'━',
	wall_horizontal_thin =			'─',
	wall_horizontal_thinleft_thickright =	'╼',
	wall_horizontal_thickleft_thinright =	'╾',
	wall_horizontal_thick_left =		'╸',
	wall_horizontal_thick_right =		'╺',
	wall_horizontal_thin_left =		'╴',
	wall_horizontal_thin_right =		'╶',
	wall_horizontal_double =		'═',
	dash =		'-',		/* equates to wall_horizontal_thick in
					 * des-files */
	underscore =	'_',		/* equates to wall_horizontal_thin in
					 * des-files */
	equals =	'=',		/* equates to wall_horizontal_double in
					 * des-files */
	pipe =		'|',		/* equates to wall_vertical_thin in
					 * des-files */

	wall_vertical_thick =			'┃',
	wall_vertical_thin =			'│',
	wall_vertical_thintop_thickbottom =	'╽',
	wall_vertical_thicktop_thinbottom =	'╿',
	wall_vertical_thick_top =		'╹',
	wall_vertical_thick_bottom =		'╻',
	wall_vertical_thin_top =		'╵',
	wall_vertical_thin_bottom =		'╷',
	wall_vertical_double =			'║',

	wall_lowerright_thick =			'┛',
	wall_lowerright_thinbottom_thickside =	'┚',
	wall_lowerright_thickbottom_thinside =	'┙',
	wall_lowerright_thin =			'┘',
	wall_lowerright_curved =		'╯',
	wall_lowerright_double =		'╝',
	wall_lowerright_doublebottom_singleside='╛',
	wall_lowerright_singlebottom_doubleside='╜',

	wall_lowerleft_thick =			'┗',
	wall_lowerleft_thinbottom_thickside =	'┖',
	wall_lowerleft_thickbottom_thinside =	'┕',
	wall_lowerleft_thin =			'└',
	wall_lowerleft_curved =			'╰',
	wall_lowerleft_double =			'╚',
	wall_lowerleft_doublebottom_singleside ='╘',
	wall_lowerleft_singlebottom_doubleside ='╙',

	wall_upperright_thick =			'┓',
	wall_upperright_thicktop_thinside =	'┑',
	wall_upperright_thintop_thickside =	'┒',
	wall_upperright_thin =			'┐',
	wall_upperright_curved =		'╮',
	wall_upperright_double =		'╗',
	wall_uperright_doubletop_singleside =	'╕',
	wall_uperright_singletop_doubleside =	'╖',

	wall_upperleft_thick =			'┏',
	wall_upperleft_thicktop_thinside =	'┍',
	wall_upperleft_thintop_thickside =	'┎',
	wall_upperleft_thin =			'┌',
	wall_upperleft_curved =			'╭',
	wall_upperleft_double =			'╔',
	wall_upperleft_doubletop_singleside =	'╒',
	wall_upperleft_singletop_doubleside =	'╓',

	wall_t_thick =					'╋',
	wall_t_thintop_thinright_thinbottom_thickleft =	'┽',
	wall_t_thintop_thickright_thinbottom_thinleft =	'┾',
	wall_t_thintop_thickright_thinbottom_thickleft ='┿',
	wall_t_thicktop_thinright_thinbottom_thinleft =	'╀',
	wall_t_thintop_thinright_thickbottom_thinleft =	'╁',
	wall_t_thicktop_thinright_thickbottom_thinleft ='╂',
	wall_t_thicktop_thinright_thinbottom_thickleft ='╃',
	wall_t_thicktop_thickright_thinbottom_thinleft ='╄',
	wall_t_thintop_thinright_thickbottom_thickleft ='╅',
	wall_t_thintop_thickright_thickbottom_thinleft ='╆',
	wall_t_thicktop_thickright_thinbottom_thickleft='╇',
	wall_t_thintop_thickright_thickbottom_thickleft='╈',
	wall_t_thicktop_thinright_thickbottom_thickleft='╉',
	wall_t_thicktop_thickright_thickbottom_thinleft='╊',
	wall_t_thin =					'┼',
	wall_t_double =					'╬',
	wall_t_doubletop_singleright_doubledown_singleleft='╫',
	wall_t_singletop_doubleright_singledown_doubleleft='╪',

	t_noleft_thick =				'┣',
	t_noleft_thintop_thickright_thickbottom =	'┢',
	t_nolefft_thicktop_thickright_thinbottom =	'┡',
	t_noleft_thicktop_thinright_thickbottom =	'┠',
	t_noleft_thintop_thinright_thickbottom =	'┟',
	t_noleft_thicktop_thinright_thinbottom =	'┞',
	t_noleft_thintop_thickright_thinbottom =	'┝',
	t_noleft_thin =					'├',

	crossing_slashes =	'╳',
	slash_forward =		'╱',
	slash_back =		'╲',
}


static immutable Glyph[] wall_horizontal = [
	Glyph.wall_horizontal_thick,			// ━
        Glyph.wall_horizontal_thin,			// ─
        Glyph.wall_horizontal_thinleft_thickright,	// ╼
        Glyph.wall_horizontal_thickleft_thinright,	// ╾
        Glyph.wall_horizontal_thick_left,		// ╸
        Glyph.wall_horizontal_thick_right,		// ╺
        Glyph.wall_horizontal_thin_left,		// ╴
        Glyph.wall_horizontal_thin_right,		// ╶
        Glyph.wall_horizontal_double,			// ═
	Glyph.dash,					// -
	Glyph.underscore,				// _
	Glyph.equals					// =
];

static immutable Glyph[] wall_vertical = [
	Glyph.pipe,					// |
	Glyph.wall_vertical_thick,			// ┃
	Glyph.wall_vertical_thin,			// │
	Glyph.wall_vertical_thintop_thickbottom,	// ╽
	Glyph.wall_vertical_thicktop_thinbottom,	// ╿
	Glyph.wall_vertical_thick_top,			// ╹
	Glyph.wall_vertical_thick_bottom,		// ╻
	Glyph.wall_vertical_thin_top,			// ╵
	Glyph.wall_vertical_thin_bottom,		// ╷
	Glyph.wall_vertical_double,			// ║
];

static immutable Glyph[] wall_lowerright = [
	Glyph.wall_lowerright_thick,			// ┛
	Glyph.wall_lowerright_thinbottom_thickside,	// ┚
	Glyph.wall_lowerright_thickbottom_thinside,	// ┙
	Glyph.wall_lowerright_thin,			// ┘
	Glyph.wall_lowerright_curved,			// ╯
	Glyph.wall_lowerright_double,			// ╝
	Glyph.wall_lowerright_doublebottom_singleside,	// ╛
	Glyph.wall_lowerright_singlebottom_doubleside,	// ╜
];

static immutable Glyph[] wall_lowerleft = [
	Glyph.wall_lowerleft_thick,			// ┗
	Glyph.wall_lowerleft_thinbottom_thickside,	// ┖
	Glyph.wall_lowerleft_thickbottom_thinside,	// ┕
	Glyph.wall_lowerleft_thin,			// └
	Glyph.wall_lowerleft_curved,			// ╰
	Glyph.wall_lowerleft_double,			// ╚
	Glyph.wall_lowerleft_doublebottom_singleside,	// ╘
	Glyph.wall_lowerleft_singlebottom_doubleside,	// ╙
];

static immutable Glyph[] wall_upperright = [
	Glyph.wall_upperright_thick,			// ┓
	Glyph.wall_upperright_thicktop_thinside,	// ┑
	Glyph.wall_upperright_thintop_thickside,	// ┒
	Glyph.wall_upperright_thin,			// ┐
	Glyph.wall_upperright_curved,			// ╮
	Glyph.wall_upperright_double,			// ╗
	Glyph.wall_uperright_doubletop_singleside,	// ╕
	Glyph.wall_uperright_singletop_doubleside,	// ╖
];

static immutable Glyph[] wall_upperleft = [
	Glyph.wall_upperleft_thick,			// ┏
	Glyph.wall_upperleft_thicktop_thinside,		// ┍
	Glyph.wall_upperleft_thintop_thickside,		// ┎
	Glyph.wall_upperleft_thin,			// ┌
	Glyph.wall_upperleft_curved,			// ╭
	Glyph.wall_upperleft_double,			// ╔
	Glyph.wall_upperleft_doubletop_singleside,	// ╒
	Glyph.wall_upperleft_singletop_doubleside	// ╓
];

static immutable Glyph[] wall_t = [
	Glyph.wall_t_thick,					// ╋
	Glyph.wall_t_thintop_thinright_thinbottom_thickleft,	// ┽
	Glyph.wall_t_thintop_thickright_thinbottom_thinleft,	// ┾
	Glyph.wall_t_thintop_thickright_thinbottom_thickleft,	// ┿
	Glyph.wall_t_thicktop_thinright_thinbottom_thinleft,	// ╀
	Glyph.wall_t_thintop_thinright_thickbottom_thinleft,	// ╁
	Glyph.wall_t_thicktop_thinright_thickbottom_thinleft,	// ╂
	Glyph.wall_t_thicktop_thinright_thinbottom_thickleft,	// ╃
	Glyph.wall_t_thicktop_thickright_thinbottom_thinleft,	// ╄
	Glyph.wall_t_thintop_thinright_thickbottom_thickleft,	// ╅
	Glyph.wall_t_thintop_thickright_thickbottom_thinleft,	// ╆
	Glyph.wall_t_thicktop_thickright_thinbottom_thickleft,	// ╇
	Glyph.wall_t_thintop_thickright_thickbottom_thickleft,	// ╈
	Glyph.wall_t_thicktop_thinright_thickbottom_thickleft,	// ╉
	Glyph.wall_t_thicktop_thickright_thickbottom_thinleft,	// ╊
	Glyph.wall_t_thin,					// ┼
	Glyph.wall_t_double,					// ╬
	Glyph.wall_t_doubletop_singleright_doubledown_singleleft,// ╫
	Glyph.wall_t_singletop_doubleright_singledown_doubleleft// ╪
];

static immutable Glyph[] all_walls = [
	Glyph.wall_horizontal_thick,			// ━
	Glyph.wall_horizontal_thin,			// ─
	Glyph.wall_horizontal_thinleft_thickright,	// ╼
	Glyph.wall_horizontal_thickleft_thinright,	// ╾
	Glyph.wall_horizontal_thick_left,		// ╸
	Glyph.wall_horizontal_thick_right,		// ╺
	Glyph.wall_horizontal_thin_left,		// ╴
	Glyph.wall_horizontal_thin_right,		// ╶
	Glyph.wall_horizontal_double,			// ═
	Glyph.dash,					/* -, equates to wall_horizontal_thick in
							 * des-files */
	Glyph.underscore,				/* _, equates to wall_horizontal_thin in
							 * des-files */
	Glyph.equals,					/* =, equates to wall_horizontal_double in
							 * des-files */

	Glyph.wall_vertical_thick,			// ┃
	Glyph.wall_vertical_thin,			// │
	Glyph.wall_vertical_thintop_thickbottom,	// ╽
	Glyph.wall_vertical_thicktop_thinbottom,	// ╿
	Glyph.wall_vertical_thick_top,			// ╹
	Glyph.wall_vertical_thick_bottom,		// ╻
	Glyph.wall_vertical_thin_top,			// ╵
	Glyph.wall_vertical_thin_bottom,		// ╷
	Glyph.wall_vertical_double,			// ║

	Glyph.wall_lowerright_thick,			// ┛
	Glyph.wall_lowerright_thinbottom_thickside,	// ┚
	Glyph.wall_lowerright_thickbottom_thinside,	// ┙
	Glyph.wall_lowerright_thin,			// ┘
	Glyph.wall_lowerright_curved,			// ╯
	Glyph.wall_lowerright_double,			// ╝
	Glyph.wall_lowerright_doublebottom_singleside,// ╛
	Glyph.wall_lowerright_singlebottom_doubleside,// ╜

	Glyph.wall_lowerleft_thick,			// ┗
	Glyph.wall_lowerleft_thinbottom_thickside,	// ┖
	Glyph.wall_lowerleft_thickbottom_thinside,	// ┕
	Glyph.wall_lowerleft_thin,			// └
	Glyph.wall_lowerleft_curved,			// ╰
	Glyph.wall_lowerleft_double,			// ╚
	Glyph.wall_lowerleft_doublebottom_singleside,	// ╘
	Glyph.wall_lowerleft_singlebottom_doubleside,	// ╙

	Glyph.wall_upperright_thick,			// ┓
	Glyph.wall_upperright_thicktop_thinside,	// ┑
	Glyph.wall_upperright_thintop_thickside,	// ┒
	Glyph.wall_upperright_thin,			// ┐
	Glyph.wall_upperright_curved,			// ╮
	Glyph.wall_upperright_double,			// ╗
	Glyph.wall_uperright_doubletop_singleside,	// ╕
	Glyph.wall_uperright_singletop_doubleside,	// ╖

	Glyph.wall_upperleft_thick,			// ┏
	Glyph.wall_upperleft_thicktop_thinside,		// ┍
	Glyph.wall_upperleft_thintop_thickside,		// ┎
	Glyph.wall_upperleft_thin,			// ┌
	Glyph.wall_upperleft_curved,			// ╭
	Glyph.wall_upperleft_double,			// ╔
	Glyph.wall_upperleft_doubletop_singleside,	// ╒
	Glyph.wall_upperleft_singletop_doubleside,	// ╓

	Glyph.wall_t_thick,					// ╋
	Glyph.wall_t_thintop_thinright_thinbottom_thickleft,	// ┽
	Glyph.wall_t_thintop_thickright_thinbottom_thinleft,	// ┾
	Glyph.wall_t_thintop_thickright_thinbottom_thickleft,	// ┿
	Glyph.wall_t_thicktop_thinright_thinbottom_thinleft,	// ╀
	Glyph.wall_t_thintop_thinright_thickbottom_thinleft,	// ╁
	Glyph.wall_t_thicktop_thinright_thickbottom_thinleft,	// ╂
	Glyph.wall_t_thicktop_thinright_thinbottom_thickleft,	// ╃
	Glyph.wall_t_thicktop_thickright_thinbottom_thinleft,	// ╄
	Glyph.wall_t_thintop_thinright_thickbottom_thickleft,	// ╅
	Glyph.wall_t_thintop_thickright_thickbottom_thinleft,	// ╆
	Glyph.wall_t_thicktop_thickright_thinbottom_thickleft,	// ╇
	Glyph.wall_t_thintop_thickright_thickbottom_thickleft,	// ╈
	Glyph.wall_t_thicktop_thinright_thickbottom_thickleft,	// ╉
	Glyph.wall_t_thicktop_thickright_thickbottom_thinleft,	// ╊
	Glyph.wall_t_thin,					// ┼
	Glyph.wall_t_double,					// ╬
	Glyph.wall_t_doubletop_singleright_doubledown_singleleft,// ╫
	Glyph.wall_t_singletop_doubleright_singledown_doubleleft,// ╪

	Glyph.t_noleft_thick,					// ┣
	Glyph.t_noleft_thintop_thickright_thickbottom,		// ┢
	Glyph.t_nolefft_thicktop_thickright_thinbottom,		// ┡
	Glyph.t_noleft_thicktop_thinright_thickbottom,		// ┠
	Glyph.t_noleft_thintop_thinright_thickbottom,		// ┟
	Glyph.t_noleft_thicktop_thinright_thinbottom,		// ┞
	Glyph.t_noleft_thintop_thickright_thinbottom,		// ┝
	Glyph.t_noleft_thin,					// ├

	Glyph.crossing_slashes,		// ╳
	Glyph.slash_forward,		// ╱
	Glyph.slash_back		// ╲
];


static immutable Glyph[] all_glyphs = [
	Glyph.space, //  
	Glyph.a, Glyph.b, Glyph.c, Glyph.d, Glyph.e, Glyph.f, Glyph.g, Glyph.h, Glyph.i, Glyph.j, Glyph.k, Glyph.l, Glyph.m, Glyph.n, Glyph.o, Glyph.p, Glyph.q, Glyph.r, Glyph.s, Glyph.t, Glyph.u, Glyph.v, Glyph.w, Glyph.x, Glyph.y, Glyph.z, Glyph.A, Glyph.B, Glyph.C, Glyph.D, Glyph.E, Glyph.F, Glyph.G, Glyph.H, Glyph.I, Glyph.J, Glyph.K, Glyph.L, Glyph.M, Glyph.N, Glyph.O, Glyph.P, Glyph.Q, Glyph.R, Glyph.S, Glyph.T, Glyph.U, Glyph.V, Glyph.W, Glyph.X, Glyph.Y, Glyph.Z,
	Glyph.middot,		// ·
	Glyph.at,		// @
	Glyph.light_square,	// ░
	Glyph.medium_square,	// ▒
	Glyph.dark_square,	// ▓
	Glyph.full_square,	// █

	Glyph.wall_horizontal_thick,			// ━
	Glyph.wall_horizontal_thin,			// ─
	Glyph.wall_horizontal_thinleft_thickright,	// ╼
	Glyph.wall_horizontal_thickleft_thinright,	// ╾
	Glyph.wall_horizontal_thick_left,		// ╸
	Glyph.wall_horizontal_thick_right,		// ╺
	Glyph.wall_horizontal_thin_left,		// ╴
	Glyph.wall_horizontal_thin_right,		// ╶
	Glyph.wall_horizontal_double,			// ═
	Glyph.dash,					// -
	Glyph.underscore,				// _
	Glyph.equals,					// =

	Glyph.wall_vertical_thick,			// ┃
	Glyph.wall_vertical_thin,			// │
	Glyph.wall_vertical_thintop_thickbottom,	// ╽
	Glyph.wall_vertical_thicktop_thinbottom,	// ╿
	Glyph.wall_vertical_thick_top,		// ╹
	Glyph.wall_vertical_thick_bottom,		// ╻
	Glyph.wall_vertical_thin_top,			// ╵
	Glyph.wall_vertical_thin_bottom,		// ╷
	Glyph.wall_vertical_double,			// ║

	Glyph.wall_lowerright_thick,			// ┛
	Glyph.wall_lowerright_thinbottom_thickside,	// ┚
	Glyph.wall_lowerright_thickbottom_thinside,	// ┙
	Glyph.wall_lowerright_thin,			// ┘
	Glyph.wall_lowerright_curved,			// ╯
	Glyph.wall_lowerright_double,			// ╝
	Glyph.wall_lowerright_doublebottom_singleside,// ╛
	Glyph.wall_lowerright_singlebottom_doubleside,// ╜

	Glyph.wall_lowerleft_thick,			// ┗
	Glyph.wall_lowerleft_thinbottom_thickside,	// ┖
	Glyph.wall_lowerleft_thickbottom_thinside,	// ┕
	Glyph.wall_lowerleft_thin,			// └
	Glyph.wall_lowerleft_curved,			// ╰
	Glyph.wall_lowerleft_double,			// ╚
	Glyph.wall_lowerleft_doublebottom_singleside,	// ╘
	Glyph.wall_lowerleft_singlebottom_doubleside,	// ╙

	Glyph.wall_upperright_thick,			// ┓
	Glyph.wall_upperright_thicktop_thinside,	// ┑
	Glyph.wall_upperright_thintop_thickside,	// ┒
	Glyph.wall_upperright_thin,			// ┐
	Glyph.wall_upperright_curved,			// ╮
	Glyph.wall_upperright_double,			// ╗
	Glyph.wall_uperright_doubletop_singleside,	// ╕
	Glyph.wall_uperright_singletop_doubleside,	// ╖

	Glyph.wall_upperleft_thick,			// ┏
	Glyph.wall_upperleft_thicktop_thinside,	// ┍
	Glyph.wall_upperleft_thintop_thickside,	// ┎
	Glyph.wall_upperleft_thin,			// ┌
	Glyph.wall_upperleft_curved,			// ╭
	Glyph.wall_upperleft_double,			// ╔
	Glyph.wall_upperleft_doubletop_singleside,	// ╒
	Glyph.wall_upperleft_singletop_doubleside,	// ╓

	Glyph.wall_t_thick,					// ╋
	Glyph.wall_t_thintop_thinright_thinbottom_thickleft,	// ┽
	Glyph.wall_t_thintop_thickright_thinbottom_thinleft,	// ┾
	Glyph.wall_t_thintop_thickright_thinbottom_thickleft,	// ┿
	Glyph.wall_t_thicktop_thinright_thinbottom_thinleft,	// ╀
	Glyph.wall_t_thintop_thinright_thickbottom_thinleft,	// ╁
	Glyph.wall_t_thicktop_thinright_thickbottom_thinleft,	// ╂
	Glyph.wall_t_thicktop_thinright_thinbottom_thickleft,	// ╃
	Glyph.wall_t_thicktop_thickright_thinbottom_thinleft,	// ╄
	Glyph.wall_t_thintop_thinright_thickbottom_thickleft,	// ╅
	Glyph.wall_t_thintop_thickright_thickbottom_thinleft,	// ╆
	Glyph.wall_t_thicktop_thickright_thinbottom_thickleft,// ╇
	Glyph.wall_t_thintop_thickright_thickbottom_thickleft,// ╈
	Glyph.wall_t_thicktop_thinright_thickbottom_thickleft,// ╉
	Glyph.wall_t_thicktop_thickright_thickbottom_thinleft,// ╊
	Glyph.wall_t_thin,					// ┼
	Glyph.wall_t_double,					// ╬
	Glyph.wall_t_doubletop_singleright_doubledown_singleleft,// ╫
	Glyph.wall_t_singletop_doubleright_singledown_doubleleft,// ╪

	Glyph.t_noleft_thick,					// ┣
	Glyph.t_noleft_thintop_thickright_thickbottom,	// ┢
	Glyph.t_nolefft_thicktop_thickright_thinbottom,	// ┡
	Glyph.t_noleft_thicktop_thinright_thickbottom,	// ┠
	Glyph.t_noleft_thintop_thinright_thickbottom,		// ┟
	Glyph.t_noleft_thicktop_thinright_thinbottom,		// ┞
	Glyph.t_noleft_thintop_thickright_thinbottom,		// ┝
	Glyph.t_noleft_thin,					// ├

	Glyph.crossing_slashes,	// ╳
	Glyph.slash_forward,		// ╱
	Glyph.slash_back,		// ╲
];
