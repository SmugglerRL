import std.experimental.logger;
import std.stdio: File;

private Logger logger;
private LogLevel min_log_level; // logs must be >this to be logged

void set_logger_target(File file) {
	logger = new FileLogger(file);
}


// unabashadly stolen from phobos

template defaultLogFunctionf(LogLevel ll) {
	void defaultLogFunctionf(int line = __LINE__, string file = __FILE__,
			string funcName = __FUNCTION__,
			string prettyFuncName = __PRETTY_FUNCTION__,
			string moduleName = __MODULE__, A...)
		(lazy string msg, lazy A args) {   
			logger.memLogFunctions!(ll).logImplf!(line, file, funcName, prettyFuncName, moduleName)(msg, args);
		}

	void defaultLogFunctionf(int line = __LINE__, string file = __FILE__,
			string funcName = __FUNCTION__,
			string prettyFuncName = __PRETTY_FUNCTION__,
			string moduleName = __MODULE__, A...)
		(lazy bool condition, lazy string msg, lazy A args) {
			logger.memLogFunctions!(ll).logImplf!(line, file, funcName, prettyFuncName, moduleName)(condition, msg, args);
		}
}
alias trace = defaultLogFunctionf!(LogLevel.trace);
alias log = defaultLogFunctionf!(LogLevel.info);
//alias info = defaultLogFunctionf!(LogLevel.info);
alias warning = defaultLogFunctionf!(LogLevel.warning);
alias error = defaultLogFunctionf!(LogLevel.error);
alias critical = defaultLogFunctionf!(LogLevel.critical);
alias fatal = defaultLogFunctionf!(LogLevel.fatal);



template defaultLogFunction(LogLevel ll) {
	void defaultLogFunction(int line = __LINE__, string file = __FILE__,
			string funcName = __FUNCTION__,
			string prettyFuncName = __PRETTY_FUNCTION__,
			string moduleName = __MODULE__, A...)
		(lazy A args)
		if ((args.length > 0 && !is(Unqual!(A[0]) : bool)) || args.length == 0) {   

			if (ll >= min_log_level) {
				logger.memLogFunctions!(ll).logImpl!(line, file, funcName, prettyFuncName, moduleName)(args);
			}
		}

	void defaultLogFunction(int line = __LINE__, string file = __FILE__,
			string funcName = __FUNCTION__,
			string prettyFuncName = __PRETTY_FUNCTION__,
			string moduleName = __MODULE__, A...)
		(lazy bool condition, lazy A args) {   
			if (ll >= min_log_level) {
				logger.memLogFunctions!(ll).logImpl!(line, file, funcName, prettyFuncName, moduleName)(condition, args);
			}
		}
}

alias logs = defaultLogFunction!(LogLevel.all);
alias traces = defaultLogFunction!(LogLevel.trace);
alias infos = defaultLogFunction!(LogLevel.info);
alias warnings = defaultLogFunction!(LogLevel.warning);
alias errors = defaultLogFunction!(LogLevel.error);
alias criticals = defaultLogFunction!(LogLevel.critical);
alias fatals = defaultLogFunction!(LogLevel.fatal);
