module item;

import colour;
import glyph;
import graphix;
import map;
import util;
import being;
import stdlib;

alias ob_cb = void delegate(ref Item self, ref Being pickerupper, ref list!Being monsters, ref Map map);

enum Item_type {
	Weapon,
	Armour,
	Fooood,
	Tooool,
	Shiiit,
	Otherr,
}


struct Item {
	Itemspec spec;
	alias spec this;

	this(Itemspec spec) {
		this.spec = spec;
	}

	Vector2n loc;
	uint wear;
}

class Itemspec {
	Item_type type;
	string name, description;
	Glyph glyph;
	ob_cb _on_pick_up;
	ob_cb _on_drop;

	@disable this();
	this(Item_type type, string name, string description, Glyph glyph, ob_cb pickup, ob_cb drop) {
		this.type = type;
		this.name = name;
		this.description = description;
		this.glyph = glyph;
		this._on_pick_up = pickup;
		this._on_drop = drop;
	}


	mixin funcwrapper!_on_pick_up;
	mixin funcwrapper!_on_drop;

}
class Toolspec: Itemspec {
	this(string name, string description, Glyph glyph, ob_cb pickup, ob_cb drop = null) {
		super(Item_type.Weapon, name, description, glyph, pickup, drop);
	}
}



Itemspec[] all_items;

shared static this() {
	all_items = [
		new Toolspec("Lamp", "What more do you want, it's a fucking lamp", Glyph.paren_left, (ref Item self, ref Being pickerupper, ref list!Being monsters, ref Map map) { pickerupper.light_dist += 4; }),
	];
}
