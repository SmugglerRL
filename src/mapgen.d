import constants;
import game;
import util;
import logging;
import map;
import rng;
import tile;

// 1/a chance of any tile being walkable
private Map map_random(int[] flags) {
	Map map = new Map();

	uint a, b;
	a = 1;
	b = 2;
	if (flags.length == 1) {
		b = flags[0];
		if (flags[0] < 0) {
			b = -b;
			a = b-1;
		}
	}
	if (flags.length == 2) {
		a = flags[0];
		b = flags[1];
	}

	map.for_all((ref Tile x) { x.base = chances(a, b) ? Tiles.floor : Tiles.rock; });

	return map;
}

private void mapcaves_postprocess(Map map) {
	foreach (y; 0 .. map_y-1) {
		foreach (x; 0 .. map_x-1) {
			if ((map[y+1, x].walkable && map[y-1, x].walkable) || (map[y, x+1].walkable && map[y, x-1].walkable)) {
				map[y, x].base = Tiles.floor;
			}
		}
	}
}

private Map real_map_caves(int[] flags, int iters = 100) {
	Map map = new Map();

	enum Direction: bool {y = true, x = false}
	enum Sign: bool {plus = true, minus = false}
	bool d = Direction.x;
	bool s = Sign.plus;

	map.for_all((ref Tile x) { x.base = Tiles.rock; });

	int tmpx, tmpy;
	// "burrow" through the cave

	tmpy = rnd(0, map_y);
	tmpx = rnd(0, map_x);
	foreach (_; 0 .. rnd(8000, 9000)) {
		foreach (__; 0 .. rnd(3, 20)) {
			foreach (i; tmpx-rnd(-1, 3)..tmpx+rnd(-1, 3)) {
				map[tmpy, i].base = Tiles.floor;
			}
			foreach (i; tmpy-rnd(-1, 3)..tmpy+rnd(-1, 3)) {
				map[i, tmpx].base = Tiles.floor;
			}



			if (d == Direction.x) {
				if (s == Sign.plus) {
					tmpx++;
				} else {
					tmpx--;
				}
			} else if (d == Direction.y) {
				if (s == Sign.plus) {
					tmpy++;
				} else {
					tmpy--;
				}
			}
		}

		if (rnd(0, 3)) {
			d = !d;
		}
		if (rnd(0, 3)) {
			s = !s;
		}
	}

/*	if (chances(iters, 1001)) {
		return real_map_caves(map, flags, iters--);
	} else {*/
		return map;
	/*}*/
}

private Map map_caves(int[] flags) {
	Map map = real_map_caves(flags);

	mapcaves_postprocess(map);
	mapcaves_postprocess(map);
	//mapcaves_postprocess(map);

	return map;
}



/* Prototypes for different types of dungeons */
enum MapType {
	random,
	caves
}

private enum int[][MapType] argnums = [MapType.random: [0, 1, 2], MapType.caves: [0]];

/* Map from a maptype to an array of ints.  Those are all the different numbers
 * of args it can take.  So, for example, it might have an option of taking 4
 * OR 5 args
 */

private void veriflags(MapType type, ulong flags) {
	bool[ulong] tmp;
	foreach (i; argnums[type]) {
		tmp[i] = false;
	}
	assert (flags in tmp);
}

Map genmap(MapType type, int[] flags = []) {
	veriflags(type, flags.length);

	switch(type) {
		case MapType.random: return map_random(flags);
		case MapType.caves: return map_caves(flags);
		default: assert(0);
	}
}
