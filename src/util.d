import std.functional: memoize;

import colour;
import constants;
import game;
import stdlib;


// provides dynamic function dispatch with objects, not classes
mixin template funcwrapper(alias func) {
	import std.traits: ReturnType;
	mixin(ReturnType!func.stringof ~ " "
			~ __traits(identifier, func)[1 .. $] // [1:$] to get rid of the leading _
			~ "(T...)(T args) {" // (Parameters!func).stringof
			~ (ReturnType!func.stringof == "void" ? "" : "return ")
			~ __traits(identifier, func) ~ " ? " // if the function exists, call it.  Otherwise, do nothing (call pass)
			~ __traits(identifier, func) ~ "(this, args)"
			~ " : " ~ (ReturnType!func.stringof == "void" ? "pass" : (ReturnType!func.stringof ~ ".init")) ~ ";"
			~ "}");
}

/*mixin template funcwrappers(funcs...) {
	static foreach (func; funcs) {
		mixin funcwrapper!func;
	}
}*/

// example:
/*
struct Bla {
	int n;

	void delegate(ref Bla self) _func;

	mixin funcwrapper!_func;
}

void main() {
	Bla bla;
	bla.n = 15;
	bla._func = (ref Bla self) {
		writefln("I have %s", self.n);
	};

	bla.func();
}
*/


class DException: Exception {
	pragma(inline, true) pure this(T...)(T args) {
		super(format(args));
	}
}

class InternalError: DException {
	pragma(inline, true) pure this(T...)(T args) {
		super(args);
	}
}

void exit(ubyte code) {
	import core.runtime: Runtime;
	static import core.stdc.stdlib;
	Runtime.terminate();
	core.stdc.stdlib.exit(code);
}

pure string fillstr(size_t length, char ch=' ') {
	string buf;
	while (length --> 0) {
		buf ~= ch;
	}
	return buf;
}



// Taken directly from http://stackoverflow.com/a/41978310
/* It's supposed to be incredibly performant...but honestly I used it because
 * I could copy-paste it easily.
 */
alias get256colour = memoize!((RGBColour colour) {
	enum ubyte[6] i2cv = [0, 0x5f, 0x87, 0xaf, 0xd7, 0xff];
	pragma(inline, true) {
		auto v2ci = (int v) => v < 48 ? 0 : v < 115 ? 1 : (v - 35) / 40;
		auto colour_index = (int r, int g, int b) => 36*r + 6*g + b;
		auto distance = (int r1, int g1, int b1, int r2, int g2, int b2) => (r1-r2)*(r1-r2) + (g1-g2)*(g1-g2) + (b1-b2)*(b1-b2);
	}

	int ir = v2ci(colour.r), ig = v2ci(colour.g), ib = v2ci(colour.b);
	int average = (colour.r + colour.g + colour.b) / 3;
	int igray = average > 238 ? 23 : (average-3) / 10;
	int cr = i2cv[ir], cg = i2cv[ig], cb = i2cv[ib];
	int gv = 8 + 10 * igray;

	int colour_err = distance(cr, cg, cb, colour.r, colour.g, colour.b);
	int gray_err   = distance(gv, gv, gv, colour.r, colour.g, colour.b);

	return cast(ubyte)(colour_err <= gray_err ? 16 + colour_index(ir, ig, ib) : 232 + igray);
});

float dist(int x1, int y1, int x2, int y2) {
	return sqrt(dist_sq(x1, y1, x2, y2));
}
int dist_sq(int x1, int y1, int x2, int y2) {
	int px = x1 - x2;
	int py = y1 - y2;
	return px * px + py * py;
}


// Overloads...
pragma(inline, true) ubyte get256colour(ubyte r, ubyte g, ubyte b) { return get256colour(RGBColour(r, g, b)); }
pragma(inline, true) ubyte get256colour(string colour) { return get256colour(RGBColour(colour)); }
