import constants;
import colour;
import glyph;
import logging;
import tile;

// (n)ormalized
struct Vector2n {
	private alias maxx = map_x, maxy = map_y;
	

	private struct Normalint(uint max) {
		private int _i;

		// No reason you would want to negate it, deref it, or invert it; unary + literally does nothing, 
		uint opUnary(string op)() if (op == "++" || op == "--") {
			mixin(op ~ "_i;");
			return _i = normalized(_i, max);
		}
		uint opBinary(string op)(int other) {
			return normalized(mixin("_i" ~ op ~ "other"), max);
		}
		uint opOpAssign(string op)(int other) {
			return i = mixin("this " ~ op ~ " other");
		}


		@property inout uint i() {
			// we don't have to normalize i.  Because assignment and inc/dec already normalize it
			return _i;
		}
		@property uint i(int newi) {
			return _i = normalized(newi, max);
		}
		alias i this;
	}
	Normalint!maxx x;
	Normalint!maxy y;

	this(uint newy, uint newx) {
		y.i = newy;
		x.i = newx;
	}

	Vector2n opBinary(string op)(Vector2n other) {
		return Vector2n(mixin("y" ~  op ~ "other.y"), mixin("x" ~ op ~ "other.x"));
	}
	private import std.traits: isIntegral;
	Vector2n opBinary(string op, T)(T other) if (isIntegral!T) {
		return Vector2n(mixin("y" ~ op ~ "other"), mixin("x" ~ op ~ "other"));
	}


	Vector2n opOpAssign(string op, T)(T other) {
		Vector2n these = this.opBinary!op(other);
		this.y = these.y;
		this.x = these.x;
		return this;
	}
}
pragma(inline, true) uint normalized(int i, int max) {
	i %= max;
	if (i < 0) {
		i += max;
	}

	return i;
	// If this function is a perf problem, use the following to trigger a cmov (compiler should be smart enough to do it that way already though)
	/*
	 * int oth = i + max;
	 * i = (i < 0) ? i : oth;
	 */
}

struct Lightpt {
	float visibility, light;
	bool visible, lit;
}

class Lightmap {
	private Lightpt[][] values;
	private Vector2n *loc;
	private uint size, sizeh;

	uint left_edge() inout {
		return loc.x - sizeh;
	}
	uint top_edge() inout {
		return loc.y - sizeh;
	}


	this(Vector2n *loc,  uint size) {
		values = new Lightpt[][](size, size);
		this.loc = loc;
		this.size = size;
		this.sizeh = size/2; // I really don't trust the optimizer.  If this were c++ I might...
	}

	Lightpt[][] get_lights() {
		return values;
	}

	//ref Lightpt opIndexAssign(Lightpt value, ulong y, ulong x) { return tiles[y][x] = value; }
	ref Lightpt opIndex(uint y, uint x) {
		uint y_coord = normalized(y - top_edge, map_y);
		uint x_coord = normalized(x - left_edge, map_x);

		assert (y_coord < size);
		assert (x_coord < size);

		return values[y_coord % map_y][x_coord % map_x];
	}
	ref Lightpt opIndex(Vector2n vec) {
		return this[vec.y, vec.x];
	}
	ref Lightpt opIndex(int y, int x) {
		return this[Vector2n(y, x)];
	}

	const Lightpt opIndex(uint y, uint x) {
		uint y_coord = normalized(y - top_edge, map_y);
		if (y_coord >= size) {
			return Lightpt(0, 0);
		}
		uint x_coord = normalized(x - left_edge, map_x);
		if (x_coord >= size) {
			return Lightpt(0, 0);
		}

		return values[y_coord][x_coord];
	}
	const Lightpt opIndex(Vector2n vec) {
		return this[vec.y, vec.x];
	}
	const Lightpt opIndex(int y, int x) {
		return this[Vector2n(y, x)];
	}

	// lambda of the form, e.g., (Tile x) => x.visible = false will return bool, not void
	// so if we take a void function, all is lost.  So take a function returning T
	void for_all(T)(T delegate(ref Lightpt x) fun) {
		foreach (ref row; values) {
			foreach (ref tile; row) {
				fun(tile);
			}
		}
	}

}
class Map {
	private Tile[][] tiles;

	this(Tile[][] tiles) {
		this.tiles = tiles;
	}
	this() {
		tiles = new Tile[][](map_x, map_y);
	}

	Tile[][] get_tiles() {
		return tiles;
	}

	// lambda of the form, e.g., (Tile x) => x.visible = false will return bool, not void
	// so if we take a void function, all is lost.  So take a function returning T
	void for_all(T)(T delegate(ref Tile x) fun) {
		foreach (ref row; tiles) {
			foreach (ref tile; row) {
				fun(tile);
			}
		}
	}

	//ref Tile opIndexAssign(Tile value, ulong y, ulong x) { return tiles[y][x] = value; }
	ref Tile opIndex(uint y, uint x) {
		return tiles[y % map_y][x % map_x];
	}
	ref Tile opIndex(Vector2n vec) {
		return tiles[vec.y][vec.x];
	}
	ref Tile opIndex(int y, int x) {
		return this[Vector2n(y, x)];
	}

	const Tile opIndex(uint y, uint x) {
		return tiles[y % map_y][x % map_x];
	}
	const Tile opIndex(Vector2n vec) {
		return tiles[vec.y][vec.x];
	}
	const Tile opIndex(int y, int x) {
		return this[Vector2n(y, x)];
	}
	
	

	Map dup() {
		Map ret = new Map();
		ret.tiles = tiles.dup;
		return ret;
	}
}
